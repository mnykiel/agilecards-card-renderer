var Nightmare = require('nightmare');
var nightmare = Nightmare({show: true});

nightmare
  .goto('file:///Users/mnykiel/Projects/agile-cards-auto/card-renderer/template.html')
  .pdf('template.pdf', {
      marginsType: 1,
      pageSize: {
          width: 62000,
          height: 100000
      }
  })
  .end()
  .then(() => {
    console.log('Done');
  })
  .catch(error => {
    console.error('Search failed:', error);
  });
