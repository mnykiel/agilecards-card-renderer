#!/usr/bin/env node

const fs = require('fs');
const http = require('http');
const connect = require('connect');
const bodyParser = require('body-parser');
const serveStatic = require('serve-static');
const handlebars = require('handlebars');
const path = require('path');
const Canvas = require('canvas');
const krCode = require('kr-code/dist/kr-code-generator');

global.document = {
    createElement: type => {
        if (type === 'canvas') {
            return new Canvas();
        }
        throw new Error('Only canvas is supported');
    }
};

const createKrCode = issue => krCode.getKrCode(3, issue.id, 300, 0).toDataURL();

const templatePath = path.resolve(__dirname, './template.html');
const template = handlebars.compile(fs.readFileSync(templatePath).toString());

const app = connect();

const prepareIssue = issue => {
    return Object.assign({}, issue, {
        estimate: issue.fields.customfield_10042 || issue.fields.customfield_10026, // haxx0r
        krCode: createKrCode(issue)
    });
}

app.use(serveStatic(__dirname));
app.use(bodyParser.json());

app.use('/', (req, res) => {
    console.log(req.body);
    res.setHeader('Content-Type', 'text/html');
    res.end(template({
        issues: req.body.issues.map(prepareIssue)
    }));
});

http.createServer(app).listen(8080);
